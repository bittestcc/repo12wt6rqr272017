# ElixirExample

Basic Elixir example app with Cowboy 2 HTTP Server & Plug.

## Installation

```bash
mix deps.get
mix run --no-halt
```

## Routes

1. `/`: 200 - Hello World
2. `/:something`: 404 - Not Found
3. `/sample/:message`: 200 - [message]

## Running

`mix local.hex --force`
`mix local.rebar --force`
`mix deps.get`
`mix compile`
`mix run --no-halt`

# IMPORTANT
  this example project is built using using docker file from this repo which
  may contain version of language different from the one you've set in Kintohub's UI

  If you want to use the version you've set in Kintohub's UI -> just remove the Dockerfile
